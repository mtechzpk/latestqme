package com.example.queueme;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class MyMessagingService extends FirebaseMessagingService {
    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        showNotification(remoteMessage.getNotification().getTitle(),remoteMessage.getNotification().getBody());
    }
    public void showNotification(String title ,String message){

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "MyNotification")
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setContentTitle(title)
                .setAutoCancel(true)
                .setContentText(message)
                .setWhen(System.currentTimeMillis());

        NotificationManagerCompat notificationManager= NotificationManagerCompat.from(this);
        notificationManager.notify(1, notificationBuilder.build());


    }
    }
