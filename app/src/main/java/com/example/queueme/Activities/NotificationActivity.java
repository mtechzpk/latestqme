package com.example.queueme.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.queueme.Adapters.NotificationAdapter;
import com.example.queueme.Models.ClerkModel;
import com.example.queueme.R;
import com.example.queueme.Server.MySingleton;
import com.example.queueme.Server.Server;
import com.example.queueme.SessionManager.SessionManager;
import com.example.queueme.Utility.Utilities;
import com.example.queueme.dialogs.Notifications;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import libs.mjn.prettydialog.PrettyDialog;

public class NotificationActivity extends AppCompatActivity {


    ImageView ivBack;
    RecyclerView recyclerView;
    ArrayList<Notifications> notifications;
    FragmentManager fragmentManager;
    String current_user_id;
    int notifyID = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);


        init();
        fragmentManager=getSupportFragmentManager();


//        SessionManager sessionManager = new SessionManager(NotificationActivity.this);
//        HashMap<String, String> user = sessionManager.getUserDetails();
//        if (user != null) {
//            current_user_id = user.get(SessionManager.USER_IDD);
//        }

        SharedPreferences mPrefs=getSharedPreferences("NotificationBadgeCount", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=mPrefs.edit();
        editor.putInt("count",0);
        editor.apply();

        notificationListApi();



//        notifications.add(new Notifications("Asad","Completd the task successfully"));
//        notifications.add(new Notifications("Asad","Completd the task successfully"));
//        notifications.add(new Notifications("Asad","Completd the task successfully"));
//        notifications.add(new Notifications("Asad","Completd the task successfully"));
//        notifications.add(new Notifications("Asad","Completd the task successfully"));


//        NotificationAdapter notificationAdapter = new NotificationAdapter(notifications,NotificationsActivity.this);
//        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(NotificationsActivity.this);
//        recyclerView.setLayoutManager(layoutManager);
//        recyclerView.setAdapter(notificationAdapter);

    }

    private void notificationListApi() {

         current_user_id=Utilities.getString(NotificationActivity.this,"id");

        final ProgressDialog progressdialog = new ProgressDialog(this);
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        progressdialog.show();
        final StringRequest RegistrationRequest = new StringRequest(Request.Method.POST, Server.BASE_URL, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    notifications = new ArrayList<>();
                    JSONObject object = new JSONObject(response);
                    String status = object.getString("success");
                    if (status.equals("1")) {

                        progressdialog.dismiss();
                        JSONArray jsonArray = object.getJSONArray("result");

                        for (int i = 0; i< jsonArray.length(); i++){

                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            String id = jsonObject.getString("id");
                            String message = jsonObject.getString("message");
                            String userid = jsonObject.getString("user_id");
                            String created_at = jsonObject.getString("created_at");
                            String shopid = jsonObject.getString("shop_id");


                            notifications.add(new Notifications(id,userid,shopid,message,created_at));




                        }

                        NotificationAdapter notificationAdapter = new NotificationAdapter(notifications,NotificationActivity.this,fragmentManager);
                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(NotificationActivity.this);
                        recyclerView.setLayoutManager(layoutManager);
                        recyclerView.setAdapter(notificationAdapter);


                    } else {
                        progressdialog.dismiss();
                        if (NotificationActivity.this != null) {
                            String error = object.getString("message");
                            new PrettyDialog(NotificationActivity.this)
                                    .setTitle(error)
                                    .setIcon(R.drawable.pdlg_icon_info)
                                    .setIconTint(R.color.colorPrimary)
//                .setMessage("PrettyDialog Message")
                                    .show();
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (NotificationActivity.this != null)
                    Toast.makeText(NotificationActivity.this, message, Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
//
                params.put("do", "notifications");
                params.put("apikey", "mtechapi12345");
                params.put("user_id",current_user_id );
                return params;
            }
        };

        RegistrationRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(NotificationActivity.this).addToRequestQueue(RegistrationRequest);
    }


    private void init() {

        recyclerView = findViewById(R.id.notification_rv);
        ivBack = findViewById(R.id.ivBack);
        ivBack.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                startActivity(new Intent(NotificationActivity.this,UserActivity.class));
                finish();
            }
        });

    }

}
