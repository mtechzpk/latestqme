package com.example.queueme.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.queueme.Adapters.UnAssignedQAdapter;
import com.example.queueme.Adapters.UserHomeStoreAdapter;
import com.example.queueme.Adapters.UserStoreAdapter;
import com.example.queueme.Fragments.UpdateProfileFragment;
import com.example.queueme.Fragments.UserStoreFragment;
import com.example.queueme.Models.UnAssignedModel;
import com.example.queueme.Models.UserHomeStoreModel;
import com.example.queueme.Models.UserStoreModel;
import com.example.queueme.R;
import com.example.queueme.Server.MySingleton;
import com.example.queueme.Server.Server;
import com.example.queueme.SessionManager.SessionManager;
import com.example.queueme.Utility.Utilities;
import com.google.android.material.navigation.NavigationView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import libs.mjn.prettydialog.PrettyDialog;
import libs.mjn.prettydialog.PrettyDialogCallback;

public class DemUserStoreActivity extends AppCompatActivity {
    private View view;
    private TextView day_open, totalUnAssignedQ, shopName;
    RecyclerView rvUserStore, rvUnAssigned;
    ImageView edit_day, ivEditTime, back_img;
    private UserStoreAdapter pAdapter;
    ArrayList<UnAssignedModel> unAssignedModels;
    private UnAssignedQAdapter uAdapter;
     Button testButton;
    private LinearLayoutManager mLayoutManager;
    ArrayList<UserStoreModel> userHomeStoreModels;
    String shopid, user_id, shop_code;
    String shop_id, error;
    public NavController navController;
    ImageView ivDrawer, ivNotification, ivProfile;
    private DrawerLayout drawer;
    NavigationView navigationView;
    RelativeLayout rlToolbar;
    TextView tvTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dem_user_store);
        initViews();
        initNavigation();
        getUserQQApi();

        String photoo = Utilities.getString(DemUserStoreActivity.this, "photo");
        String name = Utilities.getString(DemUserStoreActivity.this, "userName");
        View hView = navigationView.inflateHeaderView(R.layout.nav_header);
        ImageView photo = hView.findViewById(R.id.imageView);
        TextView user_name = hView.findViewById(R.id.user_name);
        user_name.setText(name);
        Picasso.with(DemUserStoreActivity.this).load(photoo).into(photo);
        getunAssignedApi();
    }

    private void initViews() {
        rvUserStore = findViewById(R.id.rvUserStoreActivity);
//        rvUnAssigned = view.findViewById(R.id.rvUnAssigned);
        shopName = findViewById(R.id.shop_name);
        back_img = findViewById(R.id.back_img);
        rvUnAssigned = findViewById(R.id.rvUnAssigned);
        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        ivDrawer = findViewById(R.id.ivDrawer);
        rlToolbar = findViewById(R.id.rlToolbar);
        tvTitle = findViewById(R.id.tvTitle);
        ivProfile = findViewById(R.id.ivProfile);
        String shop_image = Utilities.getString(DemUserStoreActivity.this, "shop_image");
        Picasso.with(DemUserStoreActivity.this).load(shop_image).into(ivProfile);
        ivNotification = findViewById(R.id.ivNotification);
        ivNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DemUserStoreActivity.this, NotificationActivity.class));

            }
        });


//        String namw = Utilities.getString(this, "shopname");
//        shopName.setText(namw);
//        back_img.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_userStoreFragment_to_userHomeFragment));
        back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DemUserStoreActivity.this, UserActivity.class));
                finish();
            }
        });
        testButton = findViewById(R.id.bFollow);
        testButton.setTag(1);
        testButton.setText("Follow");
        testButton.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {
                final int status = (Integer) v.getTag();
                if (status == 1) {
//                    shopid = userHomeStoreModels.get(0).getShopId();
                    getFollowApi();
                    testButton.setText("UnFollow");
                    testButton.setTextColor(R.color.white);
                    testButton.setBackgroundColor(getResources().getColor(R.color.blue));
                    v.setTag(0); //pause
                } else {
                    showCustomDialog();
//                    testButton.setText("Follow");
                    v.setTag(1); //pause
                    testButton.setClickable(false);
                }
            }

        });

        String shopname = Utilities.getString(DemUserStoreActivity.this, "shopname");

        shopName.setText(shopname);


    }

    private void initNavigation() {
        ivDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.openDrawer(GravityCompat.START, true);
            }
        });
//        navController = Navigation.findNavController(this, R.id.user_container);
//        NavigationUI.setupWithNavController(navigationView, navController);
//        NavigationUI.setupWithNavController(navigationView, navController);

        navigationView.getMenu().findItem(R.id.logout).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                showExitAppAlert();
                return true;
            }
        });
        navigationView.getMenu().findItem(R.id.userHomeFragment).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                drawer.closeDrawer(GravityCompat.START);
                return true;
            }
        });
        navigationView.getMenu().findItem(R.id.updateProfileFragment3).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                drawer.closeDrawer(GravityCompat.START);
                FragmentManager manager = getSupportFragmentManager();
                UpdateProfileFragment frag = new UpdateProfileFragment();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.replace(R.id.user_container1, frag);
                transaction.commit();


                return true;
            }
        });
        navigationView.getMenu().findItem(R.id.settingFragment).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                startActivity(new Intent(DemUserStoreActivity.this, NotificationActivity.class));
                finish();
                return true;
            }
        });
    }

    private void showExitAppAlert() {
        new AlertDialog.Builder(this)
                .setTitle("Logout")
                .setMessage("Are you sure you want to Logout?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        SessionManager sessionManager = new SessionManager(DemUserStoreActivity.this);
                        sessionManager.logoutUser();
                        Utilities.clearSharedPref(DemUserStoreActivity.this);
                    }
                })
                .setNegativeButton(android.R.string.no, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private void showCustomDialog() {
        final PrettyDialog pDialog = new PrettyDialog(DemUserStoreActivity.this);
        pDialog
                .setTitle("Message")
                .setMessage("Are you sure you want to Unfollow?")
                .setIcon(R.drawable.pdlg_icon_info)
                .setIconTint(R.color.colorPrimary)
                .addButton(
                        "Yes",
                        R.color.pdlg_color_white,
                        R.color.colorPrimary,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                getUnFollowApi();
                                pDialog.dismiss();
                            }
                        }
                )
                .addButton("No",
                        R.color.pdlg_color_white,
                        R.color.pdlg_color_red,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                pDialog.dismiss();
                            }
                        })
                .show();

    }

    private void getunAssignedApi() {

        shop_id = Utilities.getString(DemUserStoreActivity.this, "shopIdd");

        final ProgressDialog progressdialog = new ProgressDialog(DemUserStoreActivity.this);
        progressdialog.setIndeterminate(true);
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        progressdialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.anim, null));

        progressdialog.show();


        final StringRequest qmeRequest = new StringRequest(Request.Method.POST,"http://mtecsoft.com/qme/public/api/unassigned_queue_list", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    unAssignedModels = new ArrayList<>();
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("status");
                    if (status == 200) {
                        final JSONArray objUser = object.getJSONArray("data");
//
                        for (int i = 0; i < objUser.length(); i++) {
//
                            JSONObject jsonObject = objUser.getJSONObject(i);

                            String name = jsonObject.getString("queue_name");
                            String shp_id = jsonObject.getString("shop_id");
//                            String shop_name = jsonObject.getString("shop_name");
                            String noOfQ = jsonObject.getString("person");
//
                            unAssignedModels.add(new UnAssignedModel(name, noOfQ, shp_id));

                            progressdialog.dismiss();

//
                        }
                        uAdapter = new UnAssignedQAdapter(DemUserStoreActivity.this, unAssignedModels);
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(DemUserStoreActivity.this);
                        rvUnAssigned.setLayoutManager(linearLayoutManager);
                        rvUnAssigned.setAdapter(uAdapter);


                    } else {

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressdialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (DemUserStoreActivity.this != null)
                    Toast.makeText(DemUserStoreActivity.this, message, Toast.LENGTH_LONG).show();


            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("shop_id", shop_id);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Accept", "application/json");
                return params;
            }
        };

        qmeRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(DemUserStoreActivity.this).addToRequestQueue(qmeRequest);
    }

    private void getUserQQApi() {

        shop_code = Utilities.getString(DemUserStoreActivity.this, "shop_code1");

//        final ProgressDialog progressdialog = new ProgressDialog(DemUserStoreActivity.this);
//        progressdialog.setIndeterminate(true);
//        progressdialog.setMessage("Please wait..");
//        progressdialog.setCancelable(false);
//        progressdialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.anim, null));
//
//        progressdialog.show();


        final StringRequest qmeRequest = new StringRequest(Request.Method.POST,"http://mtecsoft.com/qme/public/api/queue_list_by_shop_code", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    userHomeStoreModels = new ArrayList<>();
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("status");
                    if (status == 200) {
                        final JSONArray objUser = object.getJSONArray("data");
//
                        for (int i = 0; i < objUser.length(); i++) {
//
                            JSONObject jsonObject = objUser.getJSONObject(i);

                            String name = jsonObject.getString("queue_name");
                            String shp_id = jsonObject.getString("shop_id");
                            String shop_name = jsonObject.getString("shop_name");
                            Utilities.saveString(DemUserStoreActivity.this, "shopname", shop_name);
                            String noOfQ = jsonObject.getString("person");
//
                            userHomeStoreModels.add(new UserStoreModel(name, noOfQ, shop_name, shp_id));

//                            progressdialog.dismiss();

//
                        }
                        pAdapter = new UserStoreAdapter(DemUserStoreActivity.this, userHomeStoreModels);
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(DemUserStoreActivity.this);
                        rvUserStore.setLayoutManager(linearLayoutManager);
                        rvUserStore.setAdapter(pAdapter);


                    } else {

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
//                progressdialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
//                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (DemUserStoreActivity.this != null)
                    Toast.makeText(DemUserStoreActivity.this, message, Toast.LENGTH_LONG).show();


            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("shop_code", shop_code);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Accept", "application/json");
                return params;
            }
        };

        qmeRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(DemUserStoreActivity.this).addToRequestQueue(qmeRequest);
    }

    private void getFollowApi() {

        user_id = Utilities.getString(DemUserStoreActivity.this, "id");
        shop_code = Utilities.getString(DemUserStoreActivity.this, "shop_code1");
        shopid = Utilities.getString(DemUserStoreActivity.this, "shopIdd");

        final ProgressDialog progressdialog = new ProgressDialog(DemUserStoreActivity.this);
        progressdialog.setIndeterminate(true);
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        progressdialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.anim, null));

        progressdialog.show();


        final StringRequest qmeRequest = new StringRequest(Request.Method.POST, "http://mtecsoft.com/qme/public/api/follow_shop", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    userHomeStoreModels = new ArrayList<>();
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("status");
                    if (status == 200) {
                        String message = object.getString("message");
                        progressdialog.dismiss();
                        new PrettyDialog(DemUserStoreActivity.this)
                                .setTitle(message)
                                .setIcon(R.drawable.pdlg_icon_info)
                                .setIconTint(R.color.colorPrimary)
//                .setMessage("PrettyDialog Message")
                                .show();

                    } else {
                        if (status == 400) {
                            error = object.getString("message");
                            Utilities.hideProgressDialog();
                            new PrettyDialog(DemUserStoreActivity.this)
                                    .setTitle(error)
                                    .setIcon(R.drawable.pdlg_icon_info)
                                    .setIconTint(R.color.colorPrimary)
//                .setMessage("PrettyDialog Message")
                                    .show();

                        }
                    }
//                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressdialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (DemUserStoreActivity.this != null)
                    Toast.makeText(DemUserStoreActivity.this, message, Toast.LENGTH_LONG).show();


            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", user_id);
                params.put("shop_code", shop_code);
                params.put("shop_id", shopid);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Accept", "application/json");
                return params;
            }
        };

        qmeRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(DemUserStoreActivity.this).addToRequestQueue(qmeRequest);
    }

    private void getUnFollowApi() {

        user_id = Utilities.getString(DemUserStoreActivity.this, "id");
//        shop_code = Utilities.getString(getActivity(), "editShopCode");
//        shopid = Utilities.getString(getActivity(), "shop_id");

        final ProgressDialog progressdialog = new ProgressDialog(DemUserStoreActivity.this);
        progressdialog.setIndeterminate(true);
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        progressdialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.anim, null));

        progressdialog.show();


        final StringRequest qmeRequest = new StringRequest(Request.Method.POST,"http://mtecsoft.com/qme/public/api/unfollow_shop", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    userHomeStoreModels = new ArrayList<>();
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("status");
                    if (status == 200) {
                        String message = object.getString("message");

                        progressdialog.dismiss();
                        Toast.makeText(DemUserStoreActivity.this, message, Toast.LENGTH_SHORT).show();

                    } else {
                        error = object.getString("message");
                        Utilities.hideProgressDialog();
                        new PrettyDialog(DemUserStoreActivity.this)
                                .setTitle(error)
                                .setIcon(R.drawable.pdlg_icon_info)
                                .setIconTint(R.color.colorPrimary)
//                .setMessage("PrettyDialog Message")
                                .show();
                    }
//                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressdialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (DemUserStoreActivity.this != null)
                    Toast.makeText(DemUserStoreActivity.this, message, Toast.LENGTH_LONG).show();


            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", user_id);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Accept", "application/json");
                return params;
            }
        };

        qmeRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(DemUserStoreActivity.this).addToRequestQueue(qmeRequest);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(DemUserStoreActivity.this, UserActivity.class);
        startActivity(intent);
        finish();
    }

//    @Override
//    public void onClick(View view) {
//        switch (view.getId()) {
//
//            case R.id.bUnFollow:
//                bUnFollow.setVisibility(View.GONE);
//                bFollow.setVisibility(View.VISIBLE);
//                getUnFollowApi();
//                break;
//            case R.id.bFollow:
//                shopid = userHomeStoreModels.get(0).getShopId();
//                bUnFollow.setVisibility(View.GONE);
//                bFollow.setVisibility(View.VISIBLE);
//                getFollowApi();
//                break;
//        }
//    }
}
