package com.example.queueme.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.icu.util.UniversalTimeScale;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.queueme.R;
import com.example.queueme.Utility.Utilities;
import com.google.firebase.iid.FirebaseInstanceId;

public class SplashOrignalActivity extends AppCompatActivity {
    private final String[] permissions = new String[]{Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private static final int REQUEST_CODE = 12345;
    private Button admin, clerk, user;
    private ImageView ivLogo;
    private LinearLayout llButton;
    private String user_type;
//    final String c = Utilities.getString(SplashOrignalActivity.this,"user_type");

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE) {
            if (grantResults.length == 3 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(SplashOrignalActivity.this, "Permission granted!!!", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(SplashOrignalActivity.this, "Necessary permissions not granted...", Toast.LENGTH_LONG).show();
                finish();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_orignal);
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(SplashOrignalActivity.this, instanceIdResult -> {
            String newToken = instanceIdResult.getToken();

            Utilities.saveString(SplashOrignalActivity.this,"device_token",newToken);

        });
        initViews();

        ClickViews();
        slideDownAnim();
        if (ActivityCompat.checkSelfPermission(SplashOrignalActivity.this, permissions[0]) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(SplashOrignalActivity.this, permissions[1]) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(SplashOrignalActivity.this, permissions[2]) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(SplashOrignalActivity.this, permissions, REQUEST_CODE);
        }

    }

    private void initViews() {
        admin = findViewById(R.id.admin);
        clerk = findViewById(R.id.clerk);
        user = findViewById(R.id.user);

        ivLogo = findViewById(R.id.ivLogo);
        llButton = findViewById(R.id.llButton);
        user_type = Utilities.getString(SplashOrignalActivity.this, "user_group");
    }

    private void ClickViews() {
        admin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utilities.saveString(SplashOrignalActivity.this, "login", "Admin");
                startActivity(new Intent(SplashOrignalActivity.this, LoginActivity.class));
                finish();
            }
        });
        clerk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utilities.saveString(SplashOrignalActivity.this, "login", "Clerk");
                startActivity(new Intent(SplashOrignalActivity.this, LoginActivity.class));
                finish();
            }
        });
        user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utilities.saveString(SplashOrignalActivity.this, "login", "User");
                Intent intent = new Intent(SplashOrignalActivity.this, LoginActivity.class);
//                intent.putExtra("user",c);
                startActivity(intent);
                finish();
            }
        });
    }

    private void slideDownAnim() {
        ivLogo.setVisibility(View.VISIBLE);
        Animation slideUpAnimation = AnimationUtils.loadAnimation(SplashOrignalActivity.this,
                R.anim.slide_bottom_animation);
        ivLogo.startAnimation(slideUpAnimation);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (Utilities.getString(SplashOrignalActivity.this, "login_status").equals("true")) {
                    ToNextActivity();
                } else {
//                    ivLogo.setVisibility(View.VISIBLE);
                    showButtons();
                }
            }
        }, 900);
        ivLogo.setVisibility(View.VISIBLE);
        Animation slid = AnimationUtils.loadAnimation(SplashOrignalActivity.this,
                R.anim.slide_up_animation);
        ivLogo.startAnimation(slid);
    }


    private void showButtons() {
        llButton.setVisibility(View.VISIBLE);
        Animation slideAnimation = AnimationUtils.loadAnimation(SplashOrignalActivity.this,
                R.anim.slide_left_to_right_animation);
        llButton.startAnimation(slideAnimation);
    }
    private void ToNextActivity() {
        if (!user_type.isEmpty()) {
            switch (user_type) {
                case "Admin":
                    startActivity(new Intent(SplashOrignalActivity.this, AdminActivity.class));
                    finish();
                    break;
                case "Clerk":
                    startActivity(new Intent(SplashOrignalActivity.this, LandingActivity.class));
                    finish();
                    break;
                case "User":
                    startActivity(new Intent(SplashOrignalActivity.this, UserActivity.class));
                    finish();
                    break;
            }
        } else {
//            updateUiii();
            showButtons();
        }
    }
}
