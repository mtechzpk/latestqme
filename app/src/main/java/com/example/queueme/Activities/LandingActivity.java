package com.example.queueme.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.queueme.R;
import com.example.queueme.Server.Server;
import com.example.queueme.SessionManager.SessionManager;
import com.example.queueme.Utility.Utilities;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.squareup.picasso.Picasso;

import libs.mjn.prettydialog.PrettyDialog;
import libs.mjn.prettydialog.PrettyDialogCallback;

public class LandingActivity extends AppCompatActivity {

    public NavController navController;
    ImageView ivDrawer,ivNotification;
    private DrawerLayout drawer;
    NavigationView navigationView;
    RelativeLayout rlToolbar;
    TextView tvTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);
    drawer = findViewById(R.id.drawer_layout);
    navigationView = findViewById(R.id.nav_view);
    ivDrawer = findViewById(R.id.ivDrawer);
    rlToolbar = findViewById(R.id.rlToolbar);
    tvTitle = findViewById(R.id.tvTitle);

        String photoo = Utilities.getString(LandingActivity.this, "photo");
        String name = Utilities.getString(LandingActivity.this, "userName");
        View hView = navigationView.inflateHeaderView(R.layout.nav_header);
        ImageView photo = hView.findViewById(R.id.imageView);
        TextView user_name = hView.findViewById(R.id.user_name);
        user_name.setText(name);
        Picasso.with(LandingActivity.this).load(photoo).into(photo);



    initNavigation();


}
    private void initNavigation() {
        ivDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.openDrawer(GravityCompat.START, true);
            }
        });
        navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupWithNavController(navigationView, navController);
        NavigationUI.setupWithNavController(navigationView, navController);
        navController.addOnDestinationChangedListener(new NavController.OnDestinationChangedListener() {
            @Override
            public void onDestinationChanged(@NonNull NavController controller, @NonNull NavDestination destination, @Nullable Bundle arguments) {
                if (destination.getLabel() != null) {
                    if (destination.getLabel().equals("City")) {
                        rlToolbar.setVisibility(View.GONE);
                    } else {
                        rlToolbar.setVisibility(View.VISIBLE);
                        tvTitle.setText(destination.getLabel());
                    }
                }

            }
        });

        navigationView.getMenu().findItem(R.id.logout).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                showCustomDialog();
                return true;
            }
        });
//        navigationView.getMenu().findItem(R.id.updateProfileFragment2).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
//            @Override
//            public boolean onMenuItemClick(MenuItem item) {
//                new PrettyDialog(LandingActivity.this)
//                        .setTitle("Under Developing")
//                        .setIcon(R.drawable.pdlg_icon_info)
//                        .setIconTint(R.color.colorPrimary)
////                .setMessage("PrettyDialog Message")
//                        .show();
//                return true;
//            }
//        });
    }
    private void showExitAppAlert() {
        new AlertDialog.Builder(this)
                .setTitle("Logout")
                .setMessage("Are you sure you want to Logout?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        SessionManager sessionManager = new SessionManager(LandingActivity.this);
                        sessionManager.logoutUser();
                        Utilities.clearSharedPref(LandingActivity.this);

                    }
                })
                .setNegativeButton(android.R.string.no, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
    private void showCustomDialog() {
        final PrettyDialog pDialog = new PrettyDialog(LandingActivity.this);
        pDialog
                .setTitle("Message")
                .setMessage("Are you sure you want to Logout?")
                .setIcon(R.drawable.pdlg_icon_info)
                .setIconTint(R.color.colorPrimary)
                .addButton(
                        "Yes",
                        R.color.pdlg_color_white,
                        R.color.colorPrimary,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {

                                SessionManager sessionManager = new SessionManager(LandingActivity.this);
                                sessionManager.logoutUser();
                                Utilities.clearSharedPref(LandingActivity.this);
                                pDialog.dismiss();
                            }
                        }
                )
                .addButton("No",
                        R.color.pdlg_color_white,
                        R.color.pdlg_color_red,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                pDialog.dismiss();
                            }
                        })
                .show();

    }
    private boolean flagDoubleBackToExitPressedOnce = false;
    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START))
            drawer.closeDrawers();

       else if (!navController.getCurrentDestination().getLabel().toString().equals("QMe Admin")) {
            super.onBackPressed();
        } else {

            showCustomDialog1();
        }
    }
    private void showCustomDialog1() {
        final PrettyDialog pDialog = new PrettyDialog(LandingActivity.this);
        pDialog
                .setTitle("Message")
                .setMessage("Are you sure you want to Exit?")
                .setIcon(R.drawable.pdlg_icon_info)
                .setIconTint(R.color.colorPrimary)
                .addButton(
                        "Yes",
                        R.color.colorPrimary,
                        R.color.pdlg_color_white,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                finishAffinity();
                                pDialog.dismiss();
                            }
                        }
                )
                .addButton("No",
                        R.color.pdlg_color_red,
                        R.color.pdlg_color_white,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                pDialog.dismiss();
                            }
                        })
                .show();

    }
}
