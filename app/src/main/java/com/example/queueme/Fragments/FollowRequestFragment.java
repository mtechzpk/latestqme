package com.example.queueme.Fragments;


import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.queueme.Adapters.ClerkAdapter;
import com.example.queueme.Models.ClerkModel;
import com.example.queueme.Models.FollowReqModel;
import com.example.queueme.Models.UserStoreModel;
import com.example.queueme.R;
import com.example.queueme.Server.MySingleton;
import com.example.queueme.Server.Server;
import com.example.queueme.Utility.Utilities;
import com.example.queueme.adminAdapters.FollowReqAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import libs.mjn.prettydialog.PrettyDialog;

/**
 * A simple {@link Fragment} subclass.
 */
public class FollowRequestFragment extends Fragment {

    private View view;
    private RecyclerView rvFollowReq;
    private FollowReqAdapter pAdapter;
    private LinearLayoutManager mLayoutManager;
    private ArrayList<FollowReqModel> followReqModels;
    String id, error;

    public FollowRequestFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_follow_request, container, false);
        rvFollowReq = view.findViewById(R.id.rvFollowReq);
        getFollowReqApi();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void getFollowReqApi() {

        id = Utilities.getString(getActivity(), "id");

        final ProgressDialog progressdialog = new ProgressDialog(getActivity());
        progressdialog.setIndeterminate(true);
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        progressdialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.anim, null));

        progressdialog.show();


        final StringRequest qmeRequest = new StringRequest(Request.Method.POST, "http://mtecsoft.com/qme/public/api/get_follow_requests", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    followReqModels = new ArrayList<>();
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("status");
                    if (status == 200) {

                        // JSONObject obj = object.getJSONObject("result");
                        final JSONArray objUser = object.getJSONArray("data");

                        for (int i = 0; i < objUser.length(); i++) {
//
                            JSONObject jsonObject = objUser.getJSONObject(i);

//                            String id = jsonObject.getString("id");
//                            String shopCode = jsonObject.getString("shop_code");
                            String name = jsonObject.getString("shop_name");
                            String follow_id = jsonObject.getString("follow_by");
                            String follow_status = jsonObject.getString("follow_status");

                            String image = jsonObject.getString("follower_image");


                            Utilities.saveString(getContext(), "name", name);
                            Utilities.saveString(getContext(), "follow_id", follow_id);
//                            String noOfQ = jsonObject.getString("number_of_persons");

//                            Utilities.saveString(getContext(), "clerkQListId", clerkQListId);
//                            Utilities.saveString(getContext(), "noOfPerson", noOfQ);

                            followReqModels.add(new FollowReqModel(name, image, follow_id, follow_status));

                        }
                        pAdapter = new FollowReqAdapter(getContext(), followReqModels);
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
                        rvFollowReq.setLayoutManager(linearLayoutManager);
                        rvFollowReq.setAdapter(pAdapter);


                    } else {

                        if (status == 400) {
                            error = object.getString("message");
                            Utilities.hideProgressDialog();
                            new PrettyDialog(getActivity())
                                    .setTitle(error)
                                    .setIcon(R.drawable.pdlg_icon_info)
                                    .setIconTint(R.color.colorPrimary)
//                .setMessage("PrettyDialog Message")
                                    .show();
                        }
                    }
//                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressdialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (getActivity() != null)
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();


            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("store_admin_id", id);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Accept", "application/json");
                return params;
            }
        };

        qmeRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(getActivity()).addToRequestQueue(qmeRequest);
    }

}
