package com.example.queueme.Fragments;


import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.BuildConfig;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.queueme.Activities.AdminActivity;
import com.example.queueme.Adapters.AdminStoreAdapter;
import com.example.queueme.Models.AdminStoreModel;
import com.example.queueme.R;
import com.example.queueme.Server.MySingleton;
import com.example.queueme.Server.Server;
import com.example.queueme.Utility.Utilities;
import com.example.queueme.dialogs.EditAdminShopTimes;
import com.example.queueme.dialogs.EditTimesDialog;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import libs.mjn.prettydialog.PrettyDialog;

public class AdminStoreFragment extends Fragment {
    Button bFollowReq, bEditShop;
    private View view;
    private TextView tvShopCode, tvTimeOpen, tvTimeClose, tvShopName;
    RecyclerView rv_shop_detail;
    LinearLayout edit_day, ivEditTime, llShareQR, llShareQRCode;
    ImageView back_img, ivQrCodeImage,ivProfile;
    private AdminStoreAdapter pAdapter;
    private LinearLayoutManager mLayoutManager;
    Switch sUpdateShop;
String shopimage;
    String EditTextValue, error;
    Thread thread;
    public final static int QRcodeWidth = 500;
    Bitmap bitmap;

    String shopid, files, shopCode, image, statusss, store_admin, openingTime, shop_name, closingTime, shopName, shop_code, closing_day, opening_day, closing_time, opening_time;
    private ArrayList<AdminStoreModel> adminStoreModels;

    public AdminStoreFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_qme_admin_store, container, false);
        initViews();
        clickViews();
        getAdminQApi();

        try {
            bitmap = TextToImageEncode(shopCode);

            ivQrCodeImage.setImageBitmap(bitmap);

        } catch (WriterException e) {
            e.printStackTrace();
        }
        return view;
    }

    Bitmap TextToImageEncode(String Value) throws WriterException {
        BitMatrix bitMatrix;
        try {
            bitMatrix = new MultiFormatWriter().encode(
                    Value,
                    BarcodeFormat.DATA_MATRIX.QR_CODE,
                    QRcodeWidth, QRcodeWidth, null
            );

        } catch (IllegalArgumentException Illegalargumentexception) {

            return null;
        }
        int bitMatrixWidth = bitMatrix.getWidth();

        int bitMatrixHeight = bitMatrix.getHeight();

        int[] pixels = new int[bitMatrixWidth * bitMatrixHeight];

        for (int y = 0; y < bitMatrixHeight; y++) {
            int offset = y * bitMatrixWidth;

            for (int x = 0; x < bitMatrixWidth; x++) {

                pixels[offset + x] = bitMatrix.get(x, y) ?
                        getResources().getColor(R.color.black) : getResources().getColor(R.color.white);
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_4444);

        bitmap.setPixels(pixels, 0, 500, 0, 0, bitMatrixWidth, bitMatrixHeight);
        return bitmap;
    }

    private void initViews() {
        tvTimeOpen = view.findViewById(R.id.tvTimeOpen);
        tvTimeClose = view.findViewById(R.id.tvTimeClose);
        tvShopName = view.findViewById(R.id.shop_name);
        bEditShop = view.findViewById(R.id.bEditProfile);
        sUpdateShop = view.findViewById(R.id.sUpdateShop);
        tvShopCode = view.findViewById(R.id.tvShopCode);
        llShareQR = view.findViewById(R.id.llShareQR);
        llShareQRCode = view.findViewById(R.id.llShareQRCode);
        ivProfile = view.findViewById(R.id.ivProfile);
        ivQrCodeImage = view.findViewById(R.id.ivQrCodeImage);


        shopName = Utilities.getString(getContext(), "Asshop_Name");
        store_admin = Utilities.getString(getContext(), "store_admnId");
        closingTime = Utilities.getString(getContext(), "closeTime");
        openingTime = Utilities.getString(getContext(), "openTime");
        statusss = Utilities.getString(getContext(), "statusss");
        shopCode = Utilities.getString(getContext(), "shop_code");
        shopid = Utilities.getString(getContext(), "AshopId");
        files = Utilities.getString(getContext(), "files");

        if (statusss.equals("Active")) {
            sUpdateShop.setChecked(true);
        } else if (statusss.equals("Inactive")) {
            sUpdateShop.setChecked(false);

        }
        shopimage = Utilities.getString(getContext(), "image");
        Picasso.with(getActivity()).load(shopimage).into(ivProfile);
        tvTimeClose.setText(closingTime);
        tvTimeOpen.setText(openingTime);
        tvShopName.setText(shopName);
        tvShopCode.setText("Shop Code: " + shopCode);

        rv_shop_detail = view.findViewById(R.id.rv_shop_detail);
        edit_day = view.findViewById(R.id.edit_day);
        ivEditTime = view.findViewById(R.id.ivEditTime);
        bFollowReq = view.findViewById(R.id.bFollowReq);
        back_img = view.findViewById(R.id.back_img);

        bFollowReq.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_adminStoreFragment_to_followRequestFragment));
        back_img.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_adminStoreFragment_to_adminHomeFragment));
        bEditShop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                files = Utilities.getString(getContext(), "files");
                shopName = Utilities.getString(getContext(), "Asshop_Name");
                shop_code = Utilities.getString(getContext(), "shop_code");
                shopid = Utilities.getString(getContext(), "AshopId");
                ((AdminActivity) getActivity()).navController.navigate(R.id.action_adminStoreFragment_to_updateShopFragment);
            }
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void clickViews() {
        ivEditTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                files = Utilities.getString(getContext(), "files");
                shopName = Utilities.getString(getContext(), "Asshop_Name");
                shop_code = Utilities.getString(getContext(), "shop_code");


                showFreeTrialDialog();
            }
        });
        llShareQR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareQRImage();
            }
        });
        llShareQRCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareQRCODE();
            }
        });
        sUpdateShop.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (sUpdateShop.isChecked()) {
                    sUpdateShop.setChecked(true);
                    UpdateShopStatusOnApi();
                } else {
                    sUpdateShop.setChecked(false);
                    UpdateShopStatusOffApi();

                }
            }
        });
    }

    private void showFreeTrialDialog() {
        DialogFragment newFragment = EditAdminShopTimes.newInstance(
                R.string.search_word_meaning_dialog_title);
        newFragment.show(getFragmentManager(), "dialog");

    }

    private void UpdateShopStatusOnApi() {
        shop_name = Utilities.getString(getContext(), "Asshop_Name");
        store_admin = Utilities.getString(getContext(), "store_admnId");
        closing_day = Utilities.getString(getContext(), "closeDay");
        opening_day = Utilities.getString(getContext(), "openDay");
        closing_time = Utilities.getString(getContext(), "closeTime");
        opening_time = Utilities.getString(getContext(), "openTime");
        shop_code = Utilities.getString(getContext(), "shop_code");
        image = Utilities.getString(getContext(), "image");
        final ProgressDialog progressdialog = new ProgressDialog(getActivity());
        progressdialog.setIndeterminate(true);
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        progressdialog.setIndeterminateDrawable(getActivity().getResources().getDrawable(R.drawable.anim, null));

        progressdialog.show();


        final StringRequest qmeRequest = new StringRequest(Request.Method.POST, "http://mtecsoft.com/qme/public/api/shop_update", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("status");
                    if (status == 200) {
                        String message = object.getString("message");
                        Toast.makeText(getActivity(), "Active", Toast.LENGTH_LONG);
                        new PrettyDialog(getActivity())
                                .setTitle(message)
                                .setIcon(R.drawable.pdlg_icon_info)
                                .setIconTint(R.color.colorPrimary)
//                .setMessage("PrettyDialog Message")
                                .show();
                        progressdialog.dismiss();

                    } else {
                        if (status == 400) {
                            error = object.getString("message");
                            Utilities.hideProgressDialog();
                            new PrettyDialog(getActivity())
                                    .setTitle(error)
                                    .setIcon(R.drawable.pdlg_icon_info)
                                    .setIconTint(R.color.colorPrimary)
//                .setMessage("PrettyDialog Message")
                                    .show();
                        }
                    }
//                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressdialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (getActivity() != null)
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();


            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("shop_code", shop_code);
                params.put("name", shop_name);
                params.put("store_admin", store_admin);
                params.put("image", image);
                params.put("opening_day", opening_day);
                params.put("closing_day", closing_day);
                params.put("opening_time", opening_time);
                params.put("closing_time", closing_time);
                params.put("status", "Active");
                params.put("shop_id", shopid);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Accept", "application/json");
                return params;
            }
        };

        qmeRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(getActivity()).addToRequestQueue(qmeRequest);
    }

    private void UpdateShopStatusOffApi() {

        shop_name = Utilities.getString(getContext(), "Asshop_Name");
        store_admin = Utilities.getString(getContext(), "store_admnId");
        closing_day = Utilities.getString(getContext(), "closeDay");
        opening_day = Utilities.getString(getContext(), "openDay");
        closing_time = Utilities.getString(getContext(), "closeTime");
        opening_time = Utilities.getString(getContext(), "openTime");
        shop_code = Utilities.getString(getContext(), "shop_code");
        image = Utilities.getString(getContext(), "image");

        final ProgressDialog progressdialog = new ProgressDialog(getActivity());
        progressdialog.setIndeterminate(true);
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        progressdialog.setIndeterminateDrawable(getActivity().getResources().getDrawable(R.drawable.anim, null));

        progressdialog.show();


        final StringRequest qmeRequest = new StringRequest(Request.Method.POST,"http://mtecsoft.com/qme/public/api/shop_update", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("status");
                    if (status == 200) {
                        String message = object.getString("message");
                        Toast.makeText(getActivity(), "InActive", Toast.LENGTH_LONG);
                        progressdialog.dismiss();
                        new PrettyDialog(getActivity())
                                .setTitle(message)
                                .setIcon(R.drawable.pdlg_icon_info)
                                .setIconTint(R.color.colorPrimary)
//                .setMessage("PrettyDialog Message")
                                .show();

                    } else {
                        if (status == 400) {
                            String message = object.getString("message");
                            Utilities.hideProgressDialog();
                            new PrettyDialog(getActivity())
                                    .setTitle(message)
                                    .setIcon(R.drawable.pdlg_icon_info)
                                    .setIconTint(R.color.colorPrimary)
//                .setMessage("PrettyDialog Message")
                                    .show();
                        }
                    }
//                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressdialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (getActivity() != null)
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();


            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("shop_code", shop_code);
                params.put("name", shop_name);
                params.put("store_admin", store_admin);
                params.put("image", image);
                params.put("opening_day", opening_day);
                params.put("closing_day", closing_day);
                params.put("opening_time", opening_time);
                params.put("closing_time", closing_time);
                params.put("status", "Inactive");
                params.put("shop_id", shopid);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Accept", "application/json");
                return params;
            }
        };

        qmeRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(getActivity()).addToRequestQueue(qmeRequest);
    }

    private void getAdminQApi() {

        shopid = Utilities.getString(getContext(), "AshopId");
//        clerkId = Utilities.getString(getContext(), "id");

        final ProgressDialog progressdialog = new ProgressDialog(getActivity());
        progressdialog.setIndeterminate(true);
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        progressdialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.anim, null));

        progressdialog.show();


        final StringRequest qmeRequest = new StringRequest(Request.Method.POST,"http://mtecsoft.com/qme/public/api/queue_list", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    adminStoreModels = new ArrayList<>();
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("status");
                    if (status == 200) {

                        // JSONObject obj = object.getJSONObject("result");
                        final JSONArray objUser = object.getJSONArray("data");

                        for (int i = 0; i < objUser.length(); i++) {
//
                            JSONObject jsonObject = objUser.getJSONObject(i);


                            String clerkQListId = jsonObject.getString("id");
//                            String shopCode = jsonObject.getString("shop_code");


                            String name = jsonObject.getString("queue_name");
//                            String shop_code = jsonObject.getString("shop_code");
                            String noOfPerson = jsonObject.getString("person");
//                            String store_admin = jsonObject.getString("store_admin");
//                            String shop_name = jsonObject.getString("shop_name");
                            String clerkid = jsonObject.getString("clerk_id");
//                            String clerk_name = jsonObject.getString("clerk_name");
                            String statuss = jsonObject.getString("status");
                            String shopid = jsonObject.getString("shop_id");
                            String closing_time = jsonObject.getString("closing_time");
                            String opening_time = jsonObject.getString("opening_time");

                            String closing_day = jsonObject.getString("closing_day");
                            String opening_day = jsonObject.getString("opening_day");

                            Utilities.saveString(getContext(), "clerkQListId", clerkQListId);
//                            Utilities.saveString(getContext(), "A.shop_code", shop_code);
                            Utilities.saveString(getContext(), "Qname", name);
                            Utilities.saveString(getContext(), "AnoOfPerson", noOfPerson);
                            Utilities.saveString(getContext(), "admShapName", shop_name);
                            Utilities.saveString(getContext(), "Ashopid", shopid);
                            Utilities.saveString(getContext(), "Aclerk", clerkid);
//                            Utilities.saveString(getContext(), "Aclerk_name", clerk_name);
//                            Utilities.saveString(getContext(), "store_admnId", store_admin);

                            Utilities.saveString(getContext(), "Aclosing_day", closing_day);
                            Utilities.saveString(getContext(), "Aopening_day", opening_day);

                            adminStoreModels.add(new AdminStoreModel(name, noOfPerson, clerkQListId, opening_time, closing_time, store_admin, shopid, shop_name, clerkid, statuss));

                        }
                        pAdapter = new AdminStoreAdapter(getContext(), adminStoreModels);
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
                        rv_shop_detail.setLayoutManager(linearLayoutManager);
                        rv_shop_detail.setAdapter(pAdapter);


                    } else {
                        if (status == 400) {
                            error = object.getString("message");
                            Utilities.hideProgressDialog();
                            new PrettyDialog(getActivity())
                                    .setTitle(error)
                                    .setIcon(R.drawable.pdlg_icon_info)
                                    .setIconTint(R.color.colorPrimary)
//                .setMessage("PrettyDialog Message")
                                    .show();
                        }
                    }
//                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressdialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (getActivity() != null)
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();


            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("shop_id", shopid);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Accept", "application/json");
                return params;
            }
        };

        qmeRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(getActivity()).addToRequestQueue(qmeRequest);
    }

    public void shareQRCODE() {
        try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "QME App\n");
            String shareMessage = "QME App\n Shop Name:  " + shopName;
            shareMessage = shareMessage + "\nShop Code:  " + shopCode + "";
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
            startActivity(Intent.createChooser(shareIntent, "choose one"));
        } catch (Exception e) {
            //e.toString();
        }
    }

    public void shareQRImage() {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");

        shareIntent.setType("image/*");

//         bitmap = BitmapFactory.decodeResource(bitmap);
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);

        String path =MediaStore.Images.Media.insertImage(
                getActivity().getContentResolver(), bitmap, null, null);
        Uri uri = Uri.parse(path);
        shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
        shareIntent
                .putExtra(
                        Intent.EXTRA_TEXT,
                        "Shop Name:  " + shopName + "\n Shop Code:  " + shopCode);

        startActivity(Intent.createChooser(shareIntent, "Share Via..."));

    }
//    public void shareQRImage1() {
//        Intent shareIntent = new Intent(Intent.ACTION_SEND);
//        shareIntent.setAction(Intent.ACTION_SEND);
//        shareIntent.setType("text/plain");
//
//        shareIntent.setType("image/*");
//
//        Bitmap bitmap1 = BitmapFactory.decodeResource(getResources(),R.drawable.logo);
//        ByteArrayOutputStream os = new ByteArrayOutputStream();
//        bitmap1.compress(Bitmap.CompressFormat.JPEG, 100, os);
//
//        String path = MediaStore.Images.Media.insertImage(
//                getActivity().getContentResolver(), bitmap1, null, null);
//        Uri uri = Uri.parse(path);
//        shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
//        shareIntent
//                .putExtra(
//                        Intent.EXTRA_TEXT,
//                        "**my message with URL **");
//
//        startActivity(Intent.createChooser(shareIntent, "Share Via..."));
//
//    }
}
