package com.example.queueme.Models;

public class UserHomeStoreModel {

    private String QName;
    String TotalQ;

    public UserHomeStoreModel(String QName, String totalQ) {
        this.QName = QName;
        TotalQ = totalQ;

    }

    public String getQName() {
        return QName;
    }

    public void setQName(String QName) {
        this.QName = QName;
    }


    public String getTotalQ() {
        return TotalQ;
    }

    public void setTotalQ(String totalQ) {
        TotalQ = totalQ;
    }
}
