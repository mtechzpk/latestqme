package com.example.queueme.Models;

public class ClerkListModel {
    String clerkName = "";
    int Clerkid;

//    public ClerkListModel(String clerkName, String clerkid) {
//        this.clerkName = clerkName;
//        Clerkid = clerkid;
//    }

    public int getClerkid() {
        return Clerkid;
    }

    public void setClerkid(int clerkid) {
        Clerkid = clerkid;
    }

    public String getClerkName() {
        return clerkName;
    }

    public void setClerkName(String clerkName) {
        this.clerkName = clerkName;
    }
}
