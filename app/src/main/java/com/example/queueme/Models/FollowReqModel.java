package com.example.queueme.Models;

public class FollowReqModel {

    private String Name;
    private String photo;
    String FollowId;
    String follow_status;

    public FollowReqModel(String name, String photo, String followId, String follow_status) {
        Name = name;
        this.photo = photo;
        FollowId = followId;
        this.follow_status = follow_status;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }


    public String getFollow_status() {
        return follow_status;
    }

    public void setFollow_status(String follow_status) {
        this.follow_status = follow_status;
    }

    public String getFollowId() {
        return FollowId;
    }

    public void setFollowId(String followId) {
        FollowId = followId;
    }
    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }


}
