package com.example.queueme.Models;

public class DeleteQModel {

    private String Qname;
    int Pages;

    public String getQname() {
        return Qname;
    }

    public void setQname(String qname) {
        Qname = qname;
    }

    public int getPages() {
        return Pages;
    }

    public void setPages(int pages) {
        Pages = pages;
    }
}
