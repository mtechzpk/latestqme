package com.example.queueme.Models;

public class UnAssignedModel {

    private String QName;
    String TotalQ;
    String shopId;

    public UnAssignedModel(String QName, String totalQ, String shopId) {
        this.QName = QName;
        TotalQ = totalQ;
        this.shopId = shopId;
    }

    public String getQName() {
        return QName;
    }

    public void setQName(String QName) {
        this.QName = QName;
    }

    public String getTotalQ() {
        return TotalQ;
    }

    public void setTotalQ(String totalQ) {
        TotalQ = totalQ;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }
}
