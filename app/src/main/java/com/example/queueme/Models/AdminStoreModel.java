package com.example.queueme.Models;

public class AdminStoreModel {
    private String name;
    private String NoOfPersons;
    String Qid;
    String opening_time;
    String closing_time;
    String adminid,ShopId,Shopname,clerkid,status;

    public AdminStoreModel(String name, String noOfPersons, String qid, String opening_time, String closing_time, String adminid, String shopId, String shopname, String clerkid, String status) {
        this.name = name;
        NoOfPersons = noOfPersons;
        Qid = qid;
        this.opening_time = opening_time;
        this.closing_time = closing_time;
        this.adminid = adminid;
        ShopId = shopId;
        Shopname = shopname;
        this.clerkid = clerkid;
        this.status = status;
    }

    public String getAdminid() {
        return adminid;
    }

    public void setAdminid(String adminid) {
        this.adminid = adminid;
    }

    public String getShopId() {
        return ShopId;
    }

    public void setShopId(String shopId) {
        ShopId = shopId;
    }

    public String getShopname() {
        return Shopname;
    }

    public void setShopname(String shopname) {
        Shopname = shopname;
    }

    public String getClerkid() {
        return clerkid;
    }

    public void setClerkid(String clerkid) {
        this.clerkid = clerkid;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNoOfPersons() {
        return NoOfPersons;
    }

    public void setNoOfPersons(String noOfPersons) {
        NoOfPersons = noOfPersons;
    }

    public String getQid() {
        return Qid;
    }

    public void setQid(String qid) {
        Qid = qid;
    }

    public String getOpening_time() {
        return opening_time;
    }

    public void setOpening_time(String opening_time) {
        this.opening_time = opening_time;
    }

    public String getClosing_time() {
        return closing_time;
    }

    public void setClosing_time(String closing_time) {
        this.closing_time = closing_time;
    }
}
