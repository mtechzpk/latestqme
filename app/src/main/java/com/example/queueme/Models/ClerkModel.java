package com.example.queueme.Models;

import android.widget.TextView;

public class ClerkModel {
    private String shopName;
    String noOfQ;
    String shopId;
    String store_admin_id;
    String shop_code,openTime,closeTime,openDay,closeDay;
    String image;

    public ClerkModel(String shopName, String noOfQ, String shopId, String store_admin_id, String shop_code, String openTime, String closeTime, String openDay, String closeDay, String image) {
        this.shopName = shopName;
        this.noOfQ = noOfQ;
        this.shopId = shopId;
        this.store_admin_id = store_admin_id;
        this.shop_code = shop_code;
        this.openTime = openTime;
        this.closeTime = closeTime;
        this.openDay = openDay;
        this.closeDay = closeDay;
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getOpenDay() {
        return openDay;
    }

    public void setOpenDay(String openDay) {
        this.openDay = openDay;
    }

    public String getCloseDay() {
        return closeDay;
    }

    public void setCloseDay(String closeDay) {
        this.closeDay = closeDay;
    }

    public String getOpenTime() {
        return openTime;
    }

    public void setOpenTime(String openTime) {
        this.openTime = openTime;
    }

    public String getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(String closeTime) {
        this.closeTime = closeTime;
    }

    public String getStore_admin_id() {
        return store_admin_id;
    }

    public void setStore_admin_id(String store_admin_id) {
        this.store_admin_id = store_admin_id;
    }

    public String getShop_code() {
        return shop_code;
    }

    public void setShop_code(String shop_code) {
        this.shop_code = shop_code;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getNoOfQ() {
        return noOfQ;
    }

    public void setNoOfQ(String noOfQ) {
        this.noOfQ = noOfQ;
    }



    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

}
