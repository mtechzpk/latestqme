package com.example.queueme.Models;

public class AdminHomeModel {
    String AdminShopId;
    private String AdminShopName;
    String noOfQ;
    String storeAdminId;
    String ShopCode;
    String closeTime,OpenTime,closeDay,openDay;
String status;
String image;

    public AdminHomeModel(String adminShopId, String adminShopName, String noOfQ, String storeAdminId, String shopCode, String closeTime, String openTime, String closeDay, String openDay, String status, String image) {
        AdminShopId = adminShopId;
        AdminShopName = adminShopName;
        this.noOfQ = noOfQ;
        this.storeAdminId = storeAdminId;
        ShopCode = shopCode;
        this.closeTime = closeTime;
        OpenTime = openTime;
        this.closeDay = closeDay;
        this.openDay = openDay;
        this.status = status;
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getShopCode() {
        return ShopCode;
    }

    public void setShopCode(String shopCode) {
        ShopCode = shopCode;
    }


    public String getStoreAdminId() {
        return storeAdminId;
    }

    public void setStoreAdminId(String storeAdminId) {
        this.storeAdminId = storeAdminId;
    }

    public String getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(String closeTime) {
        this.closeTime = closeTime;
    }

    public String getOpenTime() {
        return OpenTime;
    }

    public void setOpenTime(String openTime) {
        OpenTime = openTime;
    }

    public String getCloseDay() {
        return closeDay;
    }

    public void setCloseDay(String closeDay) {
        this.closeDay = closeDay;
    }

    public String getOpenDay() {
        return openDay;
    }

    public void setOpenDay(String openDay) {
        this.openDay = openDay;
    }


    public String getAdminShopId() {
        return AdminShopId;
    }

    public void setAdminShopId(String adminShopId) {
        AdminShopId = adminShopId;
    }

    public String getAdminShopName() {
        return AdminShopName;
    }

    public void setAdminShopName(String adminShopName) {
        AdminShopName = adminShopName;
    }

    public String getNoOfQ() {
        return noOfQ;
    }

    public void setNoOfQ(String noOfQ) {
        this.noOfQ = noOfQ;
    }
}
