package com.example.queueme.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.queueme.R;
import com.example.queueme.Utility.Utilities;

import libs.mjn.prettydialog.PrettyDialog;


public class PassRequirdDialog extends DialogFragment {
    ImageView ivClose;
    View view;
    EditText etPassword;
    String pass;
    Button bConfirm;

    public static PassRequirdDialog newInstance(int title) {
        PassRequirdDialog frag = new PassRequirdDialog();
        Bundle args = new Bundle();
        args.putInt("title", title);
        frag.setArguments(args);
        return frag;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_pass_requird_dialog, container);
        etPassword = view.findViewById(R.id.etPassword);
        bConfirm = view.findViewById(R.id.bConfirm);
        pass = Utilities.getString(getActivity(), "pass");
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ivClose = view.findViewById(R.id.cancel_action);
        getDialog().setCancelable(false);
        getDialog().getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().cancel();
            }
        });
        bConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String confirmPass = etPassword.getText().toString();
                if (confirmPass.equals(pass)) {
                    Utilities.saveString(getActivity(), "password", "matched");
                    getDialog().dismiss();
                    Toast.makeText(getActivity(), "Password Matched", Toast.LENGTH_SHORT).show();

                } else {
                    new PrettyDialog(getActivity())
                            .setTitle("Password not Matched ,Please Try Again")
                            .setIcon(R.drawable.pdlg_icon_info)
                            .setIconTint(R.color.colorPrimary)
//                .setMessage("PrettyDialog Message")
                            .show();

                }
            }
        });


    }

    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        getDialog().getWindow()
                .getAttributes().windowAnimations = R.style.MyAnimation_Window;
    }
}