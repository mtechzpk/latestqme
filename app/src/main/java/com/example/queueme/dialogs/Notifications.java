package com.example.queueme.dialogs;

public class Notifications {
String id,userID,shopId,mesage,createdAt;

    public Notifications(String id, String userID, String shopId, String mesage, String createdAt) {
        this.id = id;
        this.userID = userID;
        this.shopId = shopId;
        this.mesage = mesage;
        this.createdAt = createdAt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getMesage() {
        return mesage;
    }

    public void setMesage(String mesage) {
        this.mesage = mesage;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
