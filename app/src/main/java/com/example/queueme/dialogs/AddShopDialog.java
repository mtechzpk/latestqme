package com.example.queueme.dialogs;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.queueme.Activities.AdminActivity;
import com.example.queueme.Activities.DemUserStoreActivity;
import com.example.queueme.Activities.LoginActivity;
import com.example.queueme.Activities.MainActivity;
import com.example.queueme.Activities.UserActivity;
import com.example.queueme.Adapters.UserStoreAdapter;
import com.example.queueme.Fragments.UpdateProfileFragment;
import com.example.queueme.Fragments.UserStoreFragment;
import com.example.queueme.Models.UserStoreModel;
import com.example.queueme.R;
import com.example.queueme.Server.MySingleton;
import com.example.queueme.Server.Server;
import com.example.queueme.Utility.Utilities;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import libs.mjn.prettydialog.PrettyDialog;
import okhttp3.internal.Util;


public class AddShopDialog extends DialogFragment {
    ImageView ivClose;
    Button bSubmit;
    EditText etEnterCode;
    String shop_code, error;
    //    String shop_name;
    FrameLayout fl;
    MediaPlayer mp;
    SurfaceView cameraView;
    BarcodeDetector barcode;
    //    LinearLayout llsurfaceView;
    CameraSource cameraSource;
    SurfaceHolder holder;
    public static final int REQUEST_CODE = 100;
    public static final int PERMISSION_REQUEST = 200;
    public static ArrayList<UserStoreModel> userStoreModels;


    SurfaceView surfaceView;
    EditText txtBarcodeValue;
    private BarcodeDetector barcodeDetector;
    private static final int REQUEST_CAMERA_PERMISSION = 201;
    Button btnAction;
    String intentData = "";
    boolean isEmail = false;
    TextView tvQrScan;

    //qr code scanner object
    private IntentIntegrator qrScan;

    public static AddShopDialog newInstance(int title) {
        AddShopDialog frag = new AddShopDialog();
        Bundle args = new Bundle();
        args.putInt("title", title);
        frag.setArguments(args);
        return frag;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.add_shop_dialog_box, container);
        //View objects
        tvQrScan = view.findViewById(R.id.tvQrScan);
        bSubmit = view.findViewById(R.id.bSubmit);
        ivClose = view.findViewById(R.id.cancel_action);
//        cameraView = view.findViewById(R.id.cameraView1);

        txtBarcodeValue = view.findViewById(R.id.txtBarcodeValue);
        surfaceView = view.findViewById(R.id.surfaceView);
        mp = MediaPlayer.create(getActivity(), R.raw.zxing_beep);
        userStoreModels = new ArrayList<>();
//        surfaceView.getHolder().setFixedSize(200, 240);

        bSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String shopCodee = txtBarcodeValue.getText().toString();
                Utilities.saveString(getActivity(), "shop_code1", shopCodee);
//                String shopName = userStoreModels.get(0).getShopName();
//                Utilities.saveString(MainActivity.this, "shopNameee", shopName);
                if (!shopCodee.isEmpty()) {
                    mp.stop();
                    getUserQQApi(shopCodee);
//                    getDialog().cancel();
                } else {
                    showAllertDialog();
                }

//                loadFragment();


            }
        });
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDialog().cancel();
            }
        });
        return view;
    }

    private void initialiseDetectorsAndSources() {

        Toast.makeText(getContext().getApplicationContext(), "Barcode scanner started", Toast.LENGTH_SHORT).show();
        barcodeDetector = new BarcodeDetector.Builder(getContext())
                .setBarcodeFormats(Barcode.ALL_FORMATS)
                .build();

        cameraSource = new CameraSource.Builder(getActivity(), barcodeDetector)
                .setRequestedPreviewSize(1600, 800)

                .setAutoFocusEnabled(true) //you should add this feature
                .build();

        surfaceView.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {

                try {
                    if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {

                        cameraSource.start(surfaceView.getHolder());
                    } else {
                        ActivityCompat.requestPermissions(getActivity(), new
                                String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {

                cameraSource.stop();
//                mp.stop();
            }
        });


        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {
                Toast.makeText(getActivity().getApplicationContext(), "To prevent memory leaks barcode scanner has been stopped", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                final SparseArray<Barcode> barcodes = detections.getDetectedItems();
                if (barcodes.size() != 0) {
                    txtBarcodeValue.post(new Runnable() {

                        @Override
                        public void run() {
                            mp.start();
                            Vibrator v = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
// Vibrate for 500 milliseconds
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                v.vibrate(VibrationEffect.createOneShot(400, VibrationEffect.DEFAULT_AMPLITUDE));
                            } else {
                                //deprecated in API 26
                                v.vibrate(500);
                            }
                            if (barcodes.valueAt(0).email != null) {
                                mp.stop();
                                mp.release();
                                txtBarcodeValue.removeCallbacks(null);
                                intentData = barcodes.valueAt(0).email.address;
                                txtBarcodeValue.setText(intentData);
                                isEmail = true;
//                                btnAction.setText("ADD CONTENT TO THE MAIL");
                            } else {
                                isEmail = false;
//                                btnAction.setText("LAUNCH URL");
                                intentData = barcodes.valueAt(0).displayValue;
                                txtBarcodeValue.setText(intentData);
                            }
                        }
                    });
                }
            }
        });
    }


    @Override
    public void onPause() {
        super.onPause();
        mp.stop();
        mp.release();
        cameraSource.release();
    }

    @Override
    public void onResume() {
        super.onResume();
        initialiseDetectorsAndSources();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        getDialog().getWindow()
                .getAttributes().windowAnimations = R.style.DialogAnimation;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    private void getUserQQApi(final String shop_coddd) {

//        shop_code = Utilities.getString(getContext(), "editShopCode");

        final ProgressDialog progressdialog = new ProgressDialog(getActivity());
        progressdialog.setIndeterminate(true);
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        progressdialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.anim, null));

        progressdialog.show();


        final StringRequest qmeRequest = new StringRequest(Request.Method.POST,"http://mtecsoft.com/qme/public/api/queue_list_by_shop_code", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("status");
                    if (status == 200) {
                        final JSONArray objUser = object.getJSONArray("data");
                        if (objUser.length() > 0) {
                            for (int i = 0; i < objUser.length(); i++) {
//
                                JSONObject jsonObject = objUser.getJSONObject(i);

                                String name = jsonObject.getString("queue_name");
                                String shop_name = jsonObject.getString("shop_name");
                                String shp_id = jsonObject.getString("shop_id");
                                String noOfQ = jsonObject.getString("person");
                                String shop_image = jsonObject.getString("shop_image");
                                userStoreModels.add(new UserStoreModel(name, noOfQ, shop_name, shp_id));

                                Utilities.saveString(getActivity(), "shopIdd", shp_id);
                                Utilities.saveString(getActivity(), "shopname", shop_name);
                                Utilities.saveString(getActivity(), "shop_image", shop_image);

                                progressdialog.dismiss();

//
                            }
                            getDialog().cancel();
                            getDialog().dismiss();
                            Intent intent = new Intent(getActivity(), DemUserStoreActivity.class);
//                            intent.putExtra("shop_name",shop_name);
//                            String a = shop_name;
                            startActivity(intent);
                            getActivity().finish();

                        } else {
                            Utilities.hideProgressDialog();
                            Toast.makeText(getActivity(), "null", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (status == 400) {
                            error = object.getString("message");
                            Utilities.hideProgressDialog();
                            noQDialog();
                        }

//                        Toast.makeText(getActivity(), "There is no Queue", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressdialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (getActivity() != null)
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();


            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("shop_code", shop_coddd);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Accept", "application/json");
                return params;
            }
        };

        qmeRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(getActivity()).addToRequestQueue(qmeRequest);
    }

    private void showAllertDialog() {
        new PrettyDialog(getActivity())
                .setTitle("Enter shop code OR Scan QR")
                .setIcon(R.drawable.pdlg_icon_info)
                .setIconTint(R.color.colorPrimary)
//                .setMessage("PrettyDialog Message")
                .show();


    }

    private void noQDialog() {
        new PrettyDialog(getActivity())
                .setTitle(error)
                .setIcon(R.drawable.pdlg_icon_info)
                .setIconTint(R.color.colorPrimary)
//                .setMessage("PrettyDialog Message")
                .show();


    }

}