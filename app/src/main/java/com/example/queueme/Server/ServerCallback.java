package com.example.queueme.Server;

import org.json.JSONObject;

public interface ServerCallback {
    void onSuccess(JSONObject result, String ERROR);
}
