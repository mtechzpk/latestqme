package com.example.queueme.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.queueme.Models.UnAssignedModel;
import com.example.queueme.Models.UserStoreModel;
import com.example.queueme.R;

import org.json.JSONException;

import java.util.ArrayList;

public class UnAssignedQAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    private  Callback callback;
    String type;
    private ArrayList<UnAssignedModel> unAssignedModels;

    public UnAssignedQAdapter(Context context, ArrayList<UnAssignedModel> unAssignedModels) {
        this.context = context;
        this.unAssignedModels = unAssignedModels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_un_assigned, parent, false);
        return new BookViewHolder(itemView);

    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        BookViewHolder holder1 = (BookViewHolder) holder;

        holder1.tvQName.setText(unAssignedModels.get(position).getQName());
        holder1.tvTotalQ.setText(unAssignedModels.get(position).getTotalQ());

        holder1.llItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(context, "Clicked", Toast.LENGTH_SHORT).show();

            }
        });


        holder1.bind(position);
    }

    @Override
    public int getItemCount() {

        return unAssignedModels.size();
    }

    public class BookViewHolder extends RecyclerView.ViewHolder {

        TextView tvTotalQ,tvQName;
        LinearLayout llItem;

        private BookViewHolder(@NonNull View itemView) {
            super(itemView);
            tvQName = itemView.findViewById(R.id.tvQName);
            tvTotalQ = itemView.findViewById(R.id.tvTotalQ);
            llItem = itemView.findViewById(R.id.llItem);

        }

        private void bind(int pos) {
            UnAssignedModel poem = unAssignedModels.get(pos);
//            tvQName.setText(poem.getQName());
//            tvTotalQ.setText(poem.getTotalQ());
//            initClickListener();
        }

        private void initClickListener() {
//
//            llItem.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    try {
//                        callback.onItemClick(getAdapterPosition());
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                }
//            });
        }
    }

    public interface Callback {
        void onItemClick(int pos) throws JSONException;
    }
}
