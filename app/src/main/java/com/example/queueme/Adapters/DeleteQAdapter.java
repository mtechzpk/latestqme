package com.example.queueme.Adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.queueme.Models.DeleteQModel;
import com.example.queueme.R;
import com.example.queueme.Server.MySingleton;
import com.example.queueme.Server.Server;
import com.example.queueme.Utility.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DeleteQAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    String Qid;
    private Callback callback;
    private ArrayList<DeleteQModel> deleteQModels;

    public DeleteQAdapter(Context context, ArrayList<DeleteQModel> deleteQModels) {
        this.context = context;
        this.deleteQModels = deleteQModels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_delete_q, parent, false);
        return new BookViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        BookViewHolder holder1 = (BookViewHolder) holder;
//        holder1.tvName.setText(shopDetailModels.get(position).getName());
        holder1.llDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, "Deleted", Toast.LENGTH_SHORT).show();
            }
        });
        holder1.bind(position);
    }

    @Override
    public int getItemCount() {

        return deleteQModels.size();
    }

    public class BookViewHolder extends RecyclerView.ViewHolder {

        TextView tvPlus, tvMinus, tvNoOfQ, tvName;
        LinearLayout llItem,llDelete;

        private BookViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            llItem = itemView.findViewById(R.id.llItem);
            llDelete = itemView.findViewById(R.id.llDelete);

        }

        private void bind(int pos) {
            DeleteQModel poem = deleteQModels.get(pos);
            tvName.setText(poem.getQname());
//            initClickListener();
        }

        private void initClickListener() {
//
            llItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        callback.onItemClick(getAdapterPosition());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    public interface Callback {
        void onItemClick(int pos) throws JSONException;
    }

}
