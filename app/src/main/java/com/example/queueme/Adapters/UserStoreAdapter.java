package com.example.queueme.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.queueme.Models.ShopDetailModel;
import com.example.queueme.Models.UserStoreModel;
import com.example.queueme.R;
import com.example.queueme.Utility.Utilities;

import org.json.JSONException;

import java.util.ArrayList;

import libs.mjn.prettydialog.PrettyDialog;

public class UserStoreAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    private Callback callback;
    String type;
    private ArrayList<UserStoreModel> userStoreModels;

    public UserStoreAdapter(Context context, ArrayList<UserStoreModel> userStoreModels) {
        this.context = context;
        this.userStoreModels = userStoreModels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_user_store, parent, false);
        return new BookViewHolder(itemView);

    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        BookViewHolder holder1 = (BookViewHolder) holder;

        holder1.tvQName.setText(userStoreModels.get(position).getQName());
        holder1.tvTotalQ.setText(userStoreModels.get(position).getTotalQ());
        String shopName = userStoreModels.get(0).getShopName();
        Utilities.saveString(context, "shopName3", shopName);

//        holder1.tvAppointment.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showExitAppAlert();
//
//            }
//        });


        holder1.bind(position);
    }

    @Override
    public int getItemCount() {

        return userStoreModels.size();
    }

    public class BookViewHolder extends RecyclerView.ViewHolder {

        TextView tvTotalQ, tvQName,tvAppointment;
        LinearLayout llItem;

        private BookViewHolder(@NonNull View itemView) {
            super(itemView);
            tvQName = itemView.findViewById(R.id.tvQName);
            tvTotalQ = itemView.findViewById(R.id.tvTotalQ);
//            tvAppointment = itemView.findViewById(R.id.tvAppointment);

        }

        private void bind(int pos) {
            UserStoreModel poem = userStoreModels.get(pos);
//            tvQName.setText(poem.getQName());
//            tvTotalQ.setText(poem.getTotalQ());
//            initClickListener();
        }

        private void initClickListener() {
//
//            llItem.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    try {
//                        callback.onItemClick(getAdapterPosition());
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                }
//            });
        }
    }

    public interface Callback {
        void onItemClick(int pos) throws JSONException;
    }
    private void showExitAppAlert() {
        new PrettyDialog(context)
                .setTitle("Comming Soon")
                .setIcon(R.drawable.pdlg_icon_info)
                .setIconTint(R.color.colorPrimary)
//                .setMessage("PrettyDialog Message")
                .show();


    }

}
