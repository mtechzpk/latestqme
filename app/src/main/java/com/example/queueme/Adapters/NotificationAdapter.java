package com.example.queueme.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;


import com.example.queueme.Activities.UserActivity;
import com.example.queueme.R;
import com.example.queueme.SessionManager.SessionManager;
import com.example.queueme.dialogs.Notifications;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder> {

    List<Notifications> notifications;
    Context context;
    public static String owner_id;
    public static String task_id;

    FragmentManager fragmentManager;

    public NotificationAdapter(List<Notifications> notifications, Context context, FragmentManager fragmentManager) {
        this.notifications = notifications;
        this.context = context;
        this.fragmentManager = fragmentManager;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        view = layoutInflater.inflate(R.layout.notifications_adapter_layout,parent,false);
        return new NotificationAdapter.MyViewHolder(view);

    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

//        holder.imageView.setImageResource(notifications.get(position).getImage());
        holder.noti_text.setText(notifications.get(position).getMesage());
        holder.tvCreatedAt.setText(notifications.get(position).getCreatedAt());


        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "notification hitted", Toast.LENGTH_SHORT).show();
//              SessionManager.owner_id  = owner_id = notifications.get(position).getOwner_id();
//                SessionManager.taskid  = task_id = notifications.get(position).getTask_id();

//                String type = notifications.get(position).get();
//
//                if (type.equals("tasks")){
//
//                    Intent intent = new Intent(context, UserActivity.class);
//                    context.startActivity(intent);
//                }
//                else {
//
////                    DialogFragment newFragment = RatingDialogFragment.newInstance(R.string.search_word_meaning_dialog_title);
////                    newFragment.show(fragmentManager, "data");
//
//                }

            }
        });


//        holder.time.setText(earlierNotifications.get(position).getTime());

    }

    @Override
    public int getItemCount() {
        return notifications.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView noti_text,tvCreatedAt;
        LinearLayout linearLayout;




        public MyViewHolder(View itemView) {
            super(itemView);


//            imageView = itemView.findViewById(R.id.profile_img);
            noti_text = itemView.findViewById(R.id.tvNotificationMessage);
            tvCreatedAt = itemView.findViewById(R.id.tvCreatedAt);


            linearLayout = itemView.findViewById(R.id.main_layout);


//            time = itemView.findViewById(R.id.time);


        }
    }
}
