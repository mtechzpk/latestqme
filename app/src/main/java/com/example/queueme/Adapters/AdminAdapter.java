package com.example.queueme.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.queueme.Activities.AdminActivity;
import com.example.queueme.Models.AdminHomeModel;
import com.example.queueme.R;
import com.example.queueme.Utility.Utilities;
import com.example.queueme.dialogs.AddAdminQDialog;
import com.example.queueme.dialogs.AddQDialog;
import com.example.queueme.dialogs.DeleteAdminQDialog;
import com.example.queueme.dialogs.DeleteQDialog;

import org.json.JSONException;

import java.util.ArrayList;

public class AdminAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    private  Callback callback;
    String type;
    private ArrayList<AdminHomeModel> adminHomeModels;

    public AdminAdapter(Context context, ArrayList<AdminHomeModel> adminHomeModels, Callback callback) {
        this.context = context;
        this.callback=callback;
        this.adminHomeModels = adminHomeModels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_admin_home, parent, false);
        return new BookViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        BookViewHolder holder1 = (BookViewHolder) holder;
        holder1.tvShopName.setText(adminHomeModels.get(position).getAdminShopName());
        holder1.noOfQ.setText(adminHomeModels.get(position).getNoOfQ());

        holder1.tvPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String shopp_idd = adminHomeModels.get(position).getAdminShopId();
                String storeAdminId = adminHomeModels.get(position).getStoreAdminId();

                String openingTime = adminHomeModels.get(position).getOpenTime();
                String closingTime = adminHomeModels.get(position).getCloseTime();

                Utilities.saveString(context,"admin_shopp_id",shopp_idd);
                Utilities.saveString(context,"storeAdminId",storeAdminId);

                showAddQDialog();
            }
        });
        holder1.tvMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                String shopp_idd = adminHomeModels.get(position).getAdminShopId();

                Utilities.saveString(context,"admin_shopp_id",shopp_idd);
                showDeleteQDialog();
            }
        });

        holder1.llItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((AdminActivity)context).navController.navigate(R.id.action_adminHomeFragment_to_adminStoreFragment);

            }
        });


        holder1.bind(position);
    }

    @Override
    public int getItemCount() {

        return adminHomeModels.size();
    }

    public class BookViewHolder extends RecyclerView.ViewHolder {

        TextView tvPlus,tvMinus,tvShopName,noOfQ;
        LinearLayout llItem;

        private BookViewHolder(@NonNull View itemView) {
            super(itemView);
            tvMinus = itemView.findViewById(R.id.minus);
            tvPlus = itemView.findViewById(R.id.plus);
            llItem = itemView.findViewById(R.id.llItem);

            tvShopName = itemView.findViewById(R.id.tvAdminShopName);
            noOfQ = itemView.findViewById(R.id.tvNoOfQ);



        }

        private void bind(int pos) {
            AdminHomeModel poem = adminHomeModels.get(pos);
            tvShopName.setText(poem.getAdminShopName());
//            noOfQ.setText(poem.getNoOfQ());
            initClickListener();
        }

        private void initClickListener() {

            llItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        callback.onItemClick(getAdapterPosition());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    public interface Callback {
        void onItemClick(int pos) throws JSONException;
    }

    private void showAddQDialog() {
        FragmentActivity activity = (FragmentActivity) (context);
        FragmentManager fm = activity.getSupportFragmentManager();
        AddAdminQDialog alertDialog = new AddAdminQDialog();
        alertDialog.show(fm, "fragment_alert");

    }
    private void showDeleteQDialog() {
        FragmentActivity activity = (FragmentActivity) (context);
        FragmentManager fm = activity.getSupportFragmentManager();
        DeleteAdminQDialog alertDialog = new DeleteAdminQDialog();
        alertDialog.show(fm, "fragment_alert");

    }
}
