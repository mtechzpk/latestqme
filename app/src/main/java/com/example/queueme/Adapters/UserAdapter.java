package com.example.queueme.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.queueme.Activities.AdminActivity;
import com.example.queueme.Activities.UserActivity;
import com.example.queueme.Models.AdminHomeModel;
import com.example.queueme.Models.UserHomeModel;
import com.example.queueme.R;

import org.json.JSONException;

import java.util.ArrayList;

public class UserAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    private  Callback callback;
    String type;
    private ArrayList<UserHomeModel> userHomeModels;

    public UserAdapter(Context context, ArrayList<UserHomeModel> userHomeModels,Callback callback) {
        this.context = context;
        this.callback = callback;
        this.userHomeModels = userHomeModels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_user_home, parent, false);
        return new BookViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        BookViewHolder holder1 = (BookViewHolder) holder;
        holder1.tvShopName.setText(userHomeModels.get(position).getUserShopName());
        holder1.tvTotalQ.setText(userHomeModels.get(position).getTotalNoOfQ());

        holder1.llItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((UserActivity)context).navController.navigate(R.id.action_userHomeFragment_to_userStoreFragment);

            }
        });


        holder1.bind(position);
    }

    @Override
    public int getItemCount() {

        return userHomeModels.size();
    }

    public class BookViewHolder extends RecyclerView.ViewHolder {

        TextView tvShopName,tvTotalQ;
        LinearLayout llItem;

        private BookViewHolder(@NonNull View itemView) {
            super(itemView);
            tvShopName = itemView.findViewById(R.id.tvShopName);
            tvTotalQ = itemView.findViewById(R.id.tvTotalQ);
            llItem = itemView.findViewById(R.id.llItem);

        }

        private void bind(int pos) {
            UserHomeModel poem = userHomeModels.get(pos);
//            tvShopName.setText(poem.getShopName());
//            tvTotalQ.setText(poem.getTotalQ());
            initClickListener();
        }

        private void initClickListener() {
//
            llItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        callback.onItemClick(getAdapterPosition());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    public interface Callback {
        void onItemClick(int pos) throws JSONException;
    }
}
